function m=module(b)
m=0; %inizializzo a zero m
for i=1:length(b) %questo ciclo somma uno ad uno tutti i quadrati degli elementi di b a m
    m=m+b(i)^2
end
m=sqrt(m); %la radice quadrata della somma dei quadrati restituisce il modulo
    