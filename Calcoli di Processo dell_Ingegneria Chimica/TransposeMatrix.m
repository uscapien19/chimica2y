function C=TransposeMatrix(A)
[nA mA]=size(A);  %detect the size of matrix A

for i=1:nA  %two nested for cycles scan the rows and the columns
    for j=1:mA
        C(j,i)=A(i,j); %the elements of A are stored in the variable C inverting the i,j indexes
    end
end
