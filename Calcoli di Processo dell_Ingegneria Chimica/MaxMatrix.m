function max_value=MaxMatrix(A)

max_value=0; %inizializzo a 0 il valore massimo

[nA mA]=size(A);  %rilevo le dimensioni di A

for i=1:nA
    for j=1:mA  %eseguo 2 cicli for, uno per le righe e uno per le colonne
        if abs(A(i,j))>max_value  %se il valore assoluto dell'elemento A(i,j)della matrice � maggiore del valore massimo attualmente in memoria...
            max_value=abs(A(i,j)); %...rimpiazza il valore di max_value con quello nuovo
        end
    end
end
