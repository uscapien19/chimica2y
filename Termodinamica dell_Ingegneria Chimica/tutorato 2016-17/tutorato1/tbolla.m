function Tb = tbolla(T)

global A B C p x   %per recuperare le variabili dall'altro script
%tutto ci� che dipende da T devo scriverlo qua
PO(1)=(10^(A(1)+B(1)/(T+C(1))))/760; %p in atm; T in °C
PO(2)=(10^(A(2)+B(2)/(T+C(2))))/760;


Tb=PO(1)*x(1)+PO(2)*x(2)-p  %espressione di Tb in funz di T (contenuto in PO) 
%azzerandola trovo T che risolve il sistema(ho ridotto il sistema a un'unica equazione)
end