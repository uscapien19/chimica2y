close all
clear all
clc

global A B C x p Pc Tc om R
%%butano (1)
A(1)=6.83039;
B(1)=-945.9;
C(1)=240;  %oppure in un unico vettore antA 
Tc(1)=425.5 %K
Pc(1)=37.5 %atm
om(1)=0.201;

%pentano(2)
A(2)=6.85221;
B(2)=-1064.63;
C(2)=232;
Tc(2)=469.5;
Pc(2)=33.3;
om(2)=0.252;

%stechiometriche e altre
x(2)=0.25;
x(1)=1-x(2);
p=15; %atm
R=8.314 %J/mol*K

%GAS PERFETTO:
Tb=fsolve('tbolla',110)  
TR=fsolve('trugiada',100)  %azzera la funz Tb e trova T che la risolve
 
%poi inserisco T trovata in PO(1,2) e risolvo il sistema: 
%stechiometriche:                 y(1)+y(2)=1
%PER GAS PERFETTO:      fv=fl --> p*yi=PO(T)*xi  che diventa, separando i
%composti:
                                % p*y(1)=PO(1)*x(1) -->y(1)
                                % p*y(2)=PO(2)*x(2) -->y(2)

%PER VIRIALE: fv=fl ---> p*yi*phi(T,p0) =p0(T)*phi(T,p0)*xi per i=1,2
%posso usare Keq=yi/xi=p0(T)/p *phi(p0)/phi(p)

fsolve('tbollavir',110)




