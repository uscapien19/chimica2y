clc
clear all
close all

%%Dati:

global F za zb zc zw P T R P0A P0B P0C P0W DG0R 

P=0.5; %atm
T=70+273.15; %K
za=0.3;
zb=0.7;
zc=0;
zw=0;
R=8.314; %J/mol*K
F=100; %mol/h

%[A B C W] coeff di Antoine con vettore:
anta=[7.87863 7.18807 7.20211 8.10765];
antb=[-1473.11 -1416.70 -1232.83 -1750.29];
antc=[230 211 228 235];

DG0=[-37160*4.186 -87540*4.186 -54070*4.186 -71060*4.186]; %J/mol*K

P0A=(10^(anta(1)+antb(1)/(T-273.15+antc(1))))/760; %atm
P0B=(10^(anta(2)+antb(2)/(T-273.15+antc(2))))/760;
P0C=(10^(anta(3)+antb(3)/(T-273.15+antc(3))))/760;
P0W=(10^(anta(4)+antb(4)/(T-273.15+antc(4))))/760;

%calcolo DGOf, cio� della reazione come se avvenisse in fase (*), avendo i
%dati tabellati:
DG0f=DG0(4)+DG0(3)-(DG0(1)+DG0(2));
%per passare a fase liquida uso dimostrazione con pot chimico:
DG0R=DG0f+R*T*log((P0W*P0C)/(P0A*P0B));

%per risolvere il sistema imparo a memoria questa stringa:
options=optimset('display','iter','tolfun',1e-10,'maxiter',3e+3,'maxfunevals',10000);
[d,fval,exitflag]=fsolve(@flashconreazione_sistema, [50 50 15 0.25 0.25 0.25 0.25 0.25 0.25 0.25 0.25],options); %cos� risolve tenendo conto delle opzioni che ho definito prima
disp(['La portata di liquido �',num2str(d(1)),'mol/h']);%2str1 riporta il risultato numerico che ottiene facnedo girare il sistema poi ci aggiunge l'etichetta
disp(['La portata di vapore �',num2str(d(2)),'mol/h']);
disp(['il grado di vaporizzazione �',num2str(d(2)/F)]);
disp(['il grado di avanzamento lambda �',num2str(d(3))]);
disp(['La frazione molare xa di liquido �',num2str(d(4))]);
disp(['La frazione molare xb di liquido �',num2str(d(5))]);
disp(['La frazione molare xc di liquido �',num2str(d(6))]);
disp(['La frazione molare xw di liquido �',num2str(d(7))]);
disp(['La frazione molare ya di vapore �',num2str(d(8))]);
disp(['La frazione molare yb di vapore �',num2str(d(9))]);
disp(['La frazione molare yc di vapore �',num2str(d(10))]);
disp(['La frazione molare yw di vapore �',num2str(d(11))]);


