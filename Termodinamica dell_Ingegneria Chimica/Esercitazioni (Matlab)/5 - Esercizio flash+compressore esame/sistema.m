function F=sistema(X)

P=X(1);     %bar
ya=X(2);
yb=X(3);
L=X(4);
V=X(5);

T=343;  %K
Ft=100;  %mol/h
za=0.3;
xa=0.1;
xb=0.9;

P0a=10^(7.50211-1132.83/(T-273+228))*1.01325/760;    
P0b=10^(7.87863-1463.11/(T-273+230))*1.01325/760;    %bar
phi=0.5;    %%%%%%%%%%%%%%%%%%%%%%%     da modificare in base al numero matricola
gammaa=exp(phi*xb^2);
gammab=exp(phi*xa^2);

F(1)=P*ya-P0a*xa*gammaa;
F(2)=P*yb-P0b*xb*gammab;
F(3)=ya+yb-1;
F(4)=Ft*za-L*xa-V*ya;
F(5)=Ft-L-V;
