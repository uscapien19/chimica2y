function phi=phi_mix(Z,A,B,Ap,Bp,tipo)

%phi_mix calcola il coeff. di fugacit� di un composto in miscela con EoS

if tipo == 1		%vdW
end

if tipo == 2 	%RK
   phi = exp(Bp/B*(Z-1)-log(Z-B)+A/B*(Bp/B-2*sqrt(Ap/A))*log((Z+B)/Z));
end

if tipo == 3 	%RKS
   phi = exp(Bp/B*(Z-1)-log(Z-B)+A/B*(Bp/B-2*sqrt(Ap/A))*log((Z+B)/Z));
end

if tipo == 4	%PR
   phi = exp(Bp/B*(Z-1)-log(Z-B)+A/(2*sqrt(2)*B)*(Bp/B-2*sqrt(Ap/A))*log((Z+B*(1+sqrt(2)))/(Z+B*(1-sqrt(2)))));
end