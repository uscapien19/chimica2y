function F=ris_PR1(X)

global a b R T 

P=X;


%calcolo A e B 
A = (a*P)/((R*T)^2);
B = (b*P)/(R*T);

%parametri cubica
alf = -1+B;
bet = A-2*B-3*(B^2);
gam = -A*B+(B^2)+(B^3);

%Soluzione cubica

Z = roots([1 alf bet gam]);

ZR = [];
for i = 1:3
   if isreal(Z(i))
   	ZR = [ZR Z(i)];   
   end
end
sort(ZR);
Zl = min(ZR);   
Zv = max(ZR);
   

vv = (Zv*R*T)/P;
vl = (Zl*R*T)/P;

F = P*(vv - vl)-R*T*log((vv-b)/(vl-b))-(a/(b*sqrt(2)*2))*...
    log(((vl+(1-sqrt(2))*b)/((vv+(1-sqrt(2))*b)))...
    *((vv+(1+sqrt(2))*b)/(vl+(1+sqrt(2))*b)));