function y=fesame2(X)

global Tin Pin Po om R Tc Pc sR2 Cpv P T PevA PevB F zA phiA phiB phi0A phi0B

L=X(1);
V=X(2);
xA = X(3);
yA = X(4);

xB = 1-xA;
yB = 1-yA;

y(1) = F - L - V;
y(2) = F*zA - L*xA - V*yA;
y(3) = P*yA*phiA - PevA*xA*phi0A;
y(4) = P*yB*phiB - PevB*xB*phi0B;