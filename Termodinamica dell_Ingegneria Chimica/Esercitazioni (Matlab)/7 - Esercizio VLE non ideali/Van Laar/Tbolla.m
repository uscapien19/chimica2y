function Tb=Tbolla(T)

global antA antB antC gamma1 gamma2 P x

Po1=10^(antA(1)+antB(1)/(antC(1)+T));
Po2=10^(antA(2)+antB(2)/(antC(2)+T));

Tb=(Po1*x(1)*gamma1+Po2*x(2)*gamma2)/P-1;