%Diagramma Pressione - Volume EoS Peng-Robinson

close all
clear all
clc
format short 
global a b R T

%Procedura di Input
prompt = {'T (Kelvin)','Tc (Kelvin)','Pc (Bar)','S'};
dlg_title = 'Diagramma PV Isotermo - PR';
num_lines= 1;
def     = {'123','154.6','50.43','0.4069'};
datis  = inputdlg(prompt,dlg_title,num_lines,def);
dati=str2double(datis);

%Dati
T = dati(1);  %K
Tc = dati(2);  %K
Pc = dati(3)*1e5; %Bar -> Pa
S = dati(4);


Tr=T/Tc;
K= (1+S*(1-sqrt(Tr)))^2;

%Costanti
R = 8.314; %J/molK

%Parametri PR

a = (0.45724*((R*Tc)^2)*K)/Pc;
b = (0.0778*R*Tc)/Pc;

%Risoluzione Metodo 1
X = [Pc/3];
fprintf('\n Primo metodo di risoluzione - Verfica convergenza \n\n')
options=optimset('Display','Notify','Tolfun',1e-10,'MaxIter',3e+10);
sol1 = fsolve(@ris_PR1,X,options);

%Cicli per la correzione del primo tentativo
while sol1 == X && X < (Pc-10000)
    X=X+(Pc-X)/2;
clc    
fprintf('\n Convergenza non ottenuta -> Variazione primo tentativo\n\n')
    sol1 = fsolve(@ris_PR1,X,options);
end
while sol1 == X && X > 10000
    X=X-(X/2);
clc    
fprintf('\n Convergenza non ottenuta -> Variazione primo tentativo\n\n')
    sol1 = fsolve(@ris_PR1,X,options);
end


Pvs1 = sol1/1e5;

fprintf('\n Soluzione ottenuta - Tensione di vapore : %g Bar\n',Pvs1)
fprintf('\n Premere un tasto per continuare...\n')
pause

%Risoluzione Metodo 2
clc
fprintf('\n Secondo metodo di risoluzione - Verfica convergenza \n\n')
options=optimset('Display','Notify','Tolfun',1e-10,'MaxIter',3e+10);
sol2 = fsolve(@ris_PR2,X,options);
Pvs2 = sol2/1e5;
fprintf('\n Soluzione ottenuta - Tensione di vapore: %g Bar\n',Pvs2)
fprintf('\n Premere un tasto per continuare...\n')
pause

clc
fprintf('\n - Riepilogo - \n')
fprintf('\n Isoterma a %d Kelvin \n', T)
fprintf('\n Valore di primo tentativo: %g Bar \n',X/1e5)
fprintf('\n La tensione di vapore calcolata col primo metodo e'': %g Bar \n',Pvs1)
fprintf('\n La tensione di vapore calcolata col secondo metodo e'': %g Bar \n',Pvs2)


dati_g = grafico(Pvs2);
dati_g2 = grafico2(Pvs2);
fprintf('\n Coppie di valori per il sistema considerato :\n')
fprintf('\n\tPres. (Bar) \tVolume (m^3/Kmol) \n')
for m=1:1:length(dati_g')
fprintf('\t%f \t%f \n',dati_g(m,1),dati_g(m,2))
end

%Creazione grafico 1
subplot(1,2,1);
plot(dati_g(:,2),dati_g(:,1),':o')
xlabel('Volume specifico (m^3/kmol)');
ylabel('P(Bar)');
title(['Isoterma a ', num2str(T) ,' Kelvin (Totale)']);


%Creazione grafico 2
subplot(1,2,2);
plot(dati_g2(:,2),dati_g2(:,1))
xlabel('Volume specifico (m^3/kmol)');
ylabel('P(Bar)');
title(['Isoterma a ', num2str(T) ,' Kelvin (Zoom)']);


