function T=Temp(T2)


global Tc Pc om R

T1=298;
P1=1e5;
P2=4e5;

TR1=T1/Tc;
TR2=T2/Tc;
srR1=-(P1/R)*((R/Pc)*(0.675/TR1^2.6+om*0.722/TR1^5.2));
srR2=-(P2/R)*((R/Pc)*(0.675/TR2^2.6+om*0.722/TR2^5.2));

sr1=srR1*R;
sr2=srR2*R;

I=quad(@(T) 29.747./T + (25.108e-3) - (1.546e5)./T.^3, T1, T2);

T=I-R*log(P2/P1)+sr2-sr1;