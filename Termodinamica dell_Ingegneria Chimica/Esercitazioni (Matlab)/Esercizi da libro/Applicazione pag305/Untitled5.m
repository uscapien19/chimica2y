clear all
clc 
format short g

global P v no Ks Dhs hints

P=100; %bar
To=298;

R=8.314;
% 1/2 N2+ 3/2 H2 --> NH3
v=[-1/2 -3/2 1];
no=[0.25 0.75 0];

Dhf=[0 0 -45940];
Dgf=[0 0 -16401.3];

cpa=[3.28 3.249 3.578]*R;
cpb=[0.593 0.422 3.02]*R*10^-3;
cpc=[0.040 0.083 -0.816]*R*10^5;
a=cpa*v';
b=cpb*v';
c=cpc*v';
ao=cpa*no';
bo=cpb*no';
co=cpc*no';

Dho=Dhf*v';
Dgo=Dgf*v';
Ko=exp(-Dgo/(R*298))

syms t1 t2 T Ks Dhs hints
Ks=Ko*exp( int( (Dho+int(a+b*t1+c*t1^-2, t1,298,t2))/(R*t2^2) ,t2,298,T));
Dhs=Dho+int(a+b*t1+c*t1^-2, t1,298,T);
hints=int(ao+bo*t1+co*t1^-2, t1,To,T);

Xo=[450 0.15];
var=fsolve('sistema',Xo);
T=var(1)
lambda=var(2)
n=no+lambda*v;
x=n/sum(n)