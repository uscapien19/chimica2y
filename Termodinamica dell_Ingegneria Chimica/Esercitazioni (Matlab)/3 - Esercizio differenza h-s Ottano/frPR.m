function [hrRT,srR]=frPR(Z,A,B,E)

hrRT=Z-1-A/(2*sqrt(2)*B)*(1+E)*log((Z+B*(1+sqrt(2)))/(Z+B*(1-sqrt(2))));
srR=log(Z-B)-A*E/(2*sqrt(2)*B)*log((Z+B*(1+sqrt(2)))/(Z+B*(1-sqrt(2))));