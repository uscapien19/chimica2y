function f=reattore(lambda)

global P1 v no K

n=no+lambda*v;
w=n/sum(n);
Pi=P1*w;


f=K-prod(Pi.^v);
