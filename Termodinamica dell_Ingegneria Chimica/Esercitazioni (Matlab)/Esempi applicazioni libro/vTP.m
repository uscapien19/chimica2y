%vTP calcolo del volume molare a T,P con diverse EoS

format short g
clc
clear all

%dati per la sostanza
Tc=126.2; %K
Pc=33.94e5; %Pa
om=0.039; %fattore acentrico di Pitzer

%gas (fase=1), liquido (fase=2)
fase=1;

%assegno condizioni di T,P
T=170;
P=100*1.01325e5;
R=8.314;
RT=R*T;
RTc=R*Tc;
TR=T/Tc;
PR=P/Pc;

%1 gas perfetto
disp('gas perfetto ');
v=R*T/P

%2 vdW, RK, RKS, PR
eq=['vdW';'RK ';'RKS';'PR '];     %nomi EoS
for tipo=1:4                    %ciclo sulle diverse EoS
    Z=EoS(T,P,Tc,Pc,om,tipo);   %calcolo Z(P,T) 
    disp(eq(tipo, : ))              %visualizzo nome EoS    
    if fase==1                  
        v=Z(3)*RT/P            %calcolo v gas
    else
        v=Z(1)*RT/P            %calcolo v liquido
    end
end

%3 stati corrispondenti a TR
clear Z;
%vettori con i valori di PR, Z0, Z1 a TR=0.75
PRZ=[.01 .05 .1 .2 .4 .6 .8 1 1.2 1.5 2 3 5 7 10];
Z0=[.9922 .9598 .9165 .0336 .067 .1001 .133 .1656 .1981 .2426 .3260 .4823 .7854 1.0787 1.5047];
Z1=[-.0064 -.0339 -.0744 -.0143 -.0282 -.0417 -.055 -.0681 -.0808 -.0996 -.1298 -.1872 -.2929 -.3901 -.525];

Z0i=interp1(PRZ,Z0,PR);     %interpolo Z0 al valore di PR
Z1i=interp1(PRZ,Z1,PR);     %interpolo Z1 al valore di PR
Z=Z0i+om*Z1i;               %calcolo Z
disp('Stati corrispondenti ');
v=Z*RT/P

%4 viriale
B0=0.083-0.422/TR^1.6;      %calcolo B0
B1=0.139-0.172/TR^4.2;      %calcolo B1
BPcRTc=B0+om*B1;            %calcolo BPc/RTc
Z=1+BPcRTc*PR/TR;           %calcolo Z
disp('Viriale ');
v=Z*RT/P