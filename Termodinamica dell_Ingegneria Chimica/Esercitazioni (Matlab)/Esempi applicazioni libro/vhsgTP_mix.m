% calcolo volume molare e funzioni residue a T,P con diverse EoS (gas perfetto, viriale, cubiche) per miscele binarie
clc
clear all
format short g
format compact

%dati per per n-ottano
Tc(1)=569.4; %K
Pc(1)=24.97e5; %Pa
om(1)=0.398; %fattore acentrico di Pitzer
Zc(1)=0.256; %fattore compressibilitÓ critico

%dati per per n-butano
Tc(2)=425.2; 
Pc(2)=37.97e5; 
om(2)=0.199;
Zc(2)=0.274;

%gas (fase=1), liquido (fase=2)
fase=1;

%assegno condizioni di T,P e y
T=600;
P=16e5;
y(1)=0.5;
y(2)=1-y(1);
R=8.314;
RT=R*T;
RTc=R*Tc;
TR=T./Tc;
PR=P./Pc;

%1 gas perfetto
disp('gas perfetto ');
v=R*T/P
hr=0
sr=0
gr=0

%2 vdW, RK, RKS, PR
eq=['vdW';'RK ';'RKS';'PR '];     %nomi EoS
for tipo=1:4                    %ciclo sulle diverse EoS
    [Z,A,B,STrk]=EoS_mix(T,P,y,Tc,Pc,om,tipo);
    disp(eq(tipo, : ))              %visualizzo nome EoS    
    if fase==1                  
        Z(3)
        v=Z(3)*RT/P        %calcolo v gas
        [hrRT,srR]=fr(Z(3),A,B,STrk,tipo);      %calcolo hr/RT e sr/R
        hr=hrRT*RT
        sr=srR*R
        gr=hr-T*sr
    else
        Z(1)
        v=Z(1)*RT/P            %calcolo v liquido
        [hrRT,srR]=fr(Z(1),A,B,STrk,tipo);      %calcolo hr/RT e sr/R
        hr=hrRT*RT
        sr=srR*R
        gr=hr-T*sr
    end
    
end

%3 viriale
disp('Viriale ');
vc=Zc.*R.*Tc./Pc;
for i=1:2
    for j=1:2
        vcm(i,j)=((vc(i)^(1/3)+vc(j)^(1/3))/2)^3;
        Zcm(i,j)=mean([Zc(i),Zc(j)]);
        kc(i,j)=1-sqrt(vc(i)*vc(j))/vcm(i,j);
        Tcm(i,j)=sqrt(Tc(i)*Tc(j))*(1-kc(i,j));
        omm(i,j)=mean([om(i),om(j)]);
    end
end
Pcm=Zcm.*R.*Tcm./vcm;
TRm=T./Tcm;
PRm=P./Pcm;

B0ij=0.083-0.422./TRm.^1.6;      %matrice B0ij
B1ij=0.139-0.172./TRm.^4.2;      %matrice B1ij
BijPRT=(B0ij+omm.*B1ij).*PRm./TRm;            %matrice Bij*(P/RT)
BPRT=y*BijPRT*y';                %B*(P/RT) di miscela

Z=1+BPRT           %calcolo Z

dBdTijPR=(0.675./TRm.^2.6+omm*0.722./TRm.^5.2).*PRm;    %matrice dB/dTij*(P/R)
dBdTPR=y*dBdTijPR*y';       %dB/dT*(P/R) di miscela

hrRT=BPRT-dBdTPR;
srR=-dBdTPR;

v=Z*RT/P
hr=hrRT*RT
sr=srR*R
gr=hr-T*sr