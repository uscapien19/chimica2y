% Esempi di script con fsolve_HC

% R. Rota (2020)

clear all

dt=1E-2;
imet=1;

%esempio 1: Himmelblau equations
x0=[2 2];
[y RHC] = fsolve_HC(@fmain_Him, x0, dt, imet);
disp("radici  Himmelblau eqs."), disp(y)
disp("residui Himmelblau eqs."), disp(RHC)

%esempio 2: x1^2-1=0; x2^2-0.25=0; senza vincoli
x0=[-2 -2];           
[y RHC] = fsolve_HC(@fmain_neg, x0, dt, imet);
disp("radici  senza vincoli"), disp(y)
disp("residui senza vincoli"), disp(RHC)

%esempio 3: x1^2-1=0; x2^2-0.25=0; con vincoli 0<x<1
x0=[-2 -2 -2 -2];
[y RHC] = fsolve_HC(@fmain_01, x0, dt, imet);
disp("radici  con vincoli 0<x<1"), disp(y)
disp("residui con vincoli 0<x<1"), disp(RHC)
