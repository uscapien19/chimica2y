%Prima funzione

function F= freaz(PT)

global Keq Pv Q fm0 l1 l2 l3

l1=PT(1);
l2=PT(2);
l3=PT(3);

P=0.5; %atm

x0=Q.*fm0;

m(1)=x0(1)-l1;
m(2)=x0(2)-l1;
m(3)=x0(3)-l2;
m(4)=l1-l2;
m(5)=l2-l3;
x= m./sum(m);

F(1)=(P/Pv) - x(5);
F(2)=(x(4)/(x(1)*x(2)))-Keq(1);
F(3)=(x(5)/(x(3)*x(4)))-Keq(2);
