%Ossidazione acetaldeide ad acido acetico
clc
close all
clear all; clc

global F2 zl F1 zv Hc Hg Keq P

%Dati

F2=100; %[mol/s]
F1=200; %[mol/s]
P=5; %[bar]
T=470; %[K]
zv=[0 0.21 0 0 0.79 0];
zl=[1 0 0 0 0 0];

DG0R_rif=(196.4-207.2)*1e3; %[J/mol]
DH0R_rif=(229.4-232.2)*1e3; %[J/mol]
Trif=298; %[K]
R=8.314; %[J/(mol*K)]

Hc=-658+16.084*T+0.0269*T^2; %[bar]
Hg=3351+2.884*T-0.0163*T^2; %[bar]

K_rif=exp(-DG0R_rif/(R*Trif));
Keq=K_rif*exp(DH0R_rif/R*(1/Trif-1/T));

options=optimset('tolfun',1e-10,'maxiter',1e4,'maxfunevals',1e4);
[s,fval,exitflag]=fsolve(@acetaldeide,[.7 .05 .2 .05 .3 .7 185 200 42 21 50],options);

disp(['La frazione molare di acetaldeide in fase liquida � ',num2str(s(1))])
disp(['La frazione molare di ossigeno in fase liquida � ',num2str(s(2))])
disp(['La frazione molare di acido acetico in fase liquida � ',num2str(s(3))])
disp(['La frazione molare di azoto in fase liquida � ',num2str(s(4))])
disp(['La frazione molare di ossigeno in fase gas � ',num2str(s(5))])
disp(['La frazione molare di azoto in fase gas � ',num2str(s(6))])
disp(['La portata uscente della corrente in fase gas � ',num2str(s(7)),' [mol/s]'])
disp(['La portata uscente della corrente in fase liquida � ',num2str(s(8)),' [mol/s]'])

%x = 0            0      0.76229      0.23482   3.7006e-08    0.0028857
%y = 0.3601       0.6399            0            0            0            0