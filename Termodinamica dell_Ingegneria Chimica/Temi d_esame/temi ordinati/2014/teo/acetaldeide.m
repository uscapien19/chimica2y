function f=acetaldeide(x)

global F2 zl F1 zv Hc Hg Keq P

xA=x(1);
xC=x(2);
xD=x(3);
xG=x(4);
yB=x(5);
yE=x(6);
V=x(7);
L=x(8);
l=x(9);   %lambda
lb=x(10); %equilibrio fisico O2
le=x(11); %equilibrio fisico N2

f(1)=F2*zl(1)-l-L*xA;      %bilancio acetaldeide
f(2)=F1*zv(2)-lb-V*yB;     %bilancio O2gas
f(3)=lb-0.5*l-L*xC;        %bilancio O2liq ..... e cos� via
f(4)=l-L*xD;               
f(5)=F1*zv(5)-le-V*yE;    
f(6)=le-L*xG;
f(7)=xA+xC+xD+xG-1;        %stechiometriche
f(8)=yB+yE-1;
f(9)=xD/(xA*xC^0.5)-Keq;   %equilibri
f(10)=xC*Hc-P*yB;
f(11)=xG*Hg-P*yE;