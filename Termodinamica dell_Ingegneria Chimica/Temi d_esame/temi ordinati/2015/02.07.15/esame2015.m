close all 
clear all
clc
format shortg
global n0 nu G0RrifRT w 
%% REATTORE
%dati:
T0=600; %K
F=50; %mol/h  (fase vapore)
% [A B P I]
z=[0.25 0.15 0 0.6];
p1=1*1e5; %Pa
n0=F*z; 
nu=[-1 -1 1 0];

Trif=298.15; %K
prif=1; %bar
R=8.314; %J/mol K

%Kirkoff:
H0frif=[-70.95 -94.47 -310.52 -45.21]; %J/mol
alfaCp=[5.45 11.23 4.31 15.32];
betaCp=[0.021 0.0048 0.0016 0.0072];
syms T t
Cp=(alfaCp+betaCp*T)*4.186; %J/mol K  funzione di T
H0Rrif=sum(H0frif.*nu);
DeltaCpdiT=sum(Cp.*nu); %funzione di T
H0Rdit=H0Rrif+int(DeltaCpdiT,T,Trif,t); %funzione di t
integranda=H0Rdit/(R*t^2); %funz di t

%Van't Hoff:
syms T1
G0frif=[-71.74 -88.52 -406.33 -51.12];
G0Rrif=sum(G0frif.*nu);
G0RrifRT=G0Rrif/(R*Trif);
%Keq_VHrif=exp(-G0RrifRT); %oppure uso l'altra equaz di Van'tHoff
I_sim=int(integranda,t,Trif,T1); %funzione di T1 
I=vpa(I_sim); %copio nella function perch� � quella che voglio azzerare

%per bilanco di energia:
M_sim=int(DeltaCpdiT,T,Trif,T1);
M=vpa(M_sim);
H0RdiT1=H0Rrif+M; %copio anche questo valore nella function
S_sim=int(sum(n0.*Cp),T,T0,T1); %funzione di T1
S=vpa(S_sim);  %copio anche questo valore nella function

%risolvo sistema:
options=optimset('tolfun',1e-12,'maxiter',10000,'maxfunevals',10000);
X=fsolve('esame2015_sistema',[7 600],options);

l=X(1)
T1=X(2)
w
sum(w)


