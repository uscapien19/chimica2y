function f=sist(x)

global P ya yb za zb F1 
xa=x(1);
xb=x(2);
V=x(3);
L=x(4);
T=x(5);

antA=[7.98097 7.91310];
antB=[1582.271 1730.630];
antC=[239.726 233.426];

Pao=10^(antA(1)-antB(1)/(T+antC(1)))/760; %Po=bar %T=�C
Pbo=10^(antA(2)-antB(2)/(T+antC(2)))/760; %Po=bar %T=�C

f(1)=P*ya-Pao*xa;
f(2)=P*yb-Pbo*xb;
f(3)=F1*za-L*xa-V*ya;
f(4)=F1*zb-L*xb-V*yb;
%f(5)=xa+xb-1;
f(5)=F1-V-L;




