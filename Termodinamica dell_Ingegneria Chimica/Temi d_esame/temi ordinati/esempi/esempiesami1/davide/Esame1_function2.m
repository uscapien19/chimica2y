function[F]=Esame1_function2(l);
global V y F2 Keq P
wa=(V*y(1)-2*l)/(V+F2-2*l);
wb=(V*y(2))/(V+F2-2*l);
wc=(F2-l)/(V+F2-2*l);
wd=l/(V+F2-2*l);
F=1/P^2*(wd/(wc*wa^2))-Keq;