close all
clear all
clc
global P y ant1 ant2 ant3 V L Keq F2 F1
%% DATI
R=8.314;
F1=10;  %kmol/h
F2=2;
z=[0.6 0.4];
P=2.5;    %bar
y=[0.7 0.3];
ant1=[7.98097 7.91310];
ant2=[1582.271 1730.630];
ant3=[239.726 233.426];
DG0R=-12000;
%Temperatura di flash
T=fsolve('Esame1_function',110,optimset('MaxFunEval',9000,'MaxIter',9000,'TolFun',1e-20));
T=T+273.15
P0=10.^(ant1-ant2./(ant3+T-273.15))*133.322e-5; 
x=P*y./P0
L=F1*(z(2)-y(2))/(x(2)-y(2))
V=F1-L

%% Reattore
%Costante di equilibrio
Keq=exp(-DG0R/(R*T));
l=fsolve('Esame1_function2',1);
w(1)=(V*y(1)-2*l)/(V+F2-2*l);
w(2)=(V*y(2))/(V+F2-2*l);
w(3)=(F2-l)/(V+F2-2*l);
w(4)=l/(V+F2-2*l);
w
G=V+F2-2*l
