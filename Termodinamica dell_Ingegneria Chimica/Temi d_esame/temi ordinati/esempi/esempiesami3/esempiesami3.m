clear all
close all
clc
format shortg
global n0 nu P2 G0RRT y
%% COMPRESSORE
F1=100; %[mol/s]
P(1)=1*1e5; %Pa
P(2)=12*1e5;
T=[350 400]; %K
%dati: A=1, B=2, C=3
R=8.314; %[J/mol K]
Tc=[190.7 417.0 554.0]; %K
Pc=[46.41 77.11 45.60]*1e5; %Pa
om=[0.290 0.276 0.294]; %vettore, riferito ai 3 composti es. om(1)=omA
alfaCp=[11.3 15.4 57.6]; %[J/mol K]
betaCp=[0.0141 0.0937 0.434]; %[J/mol K2]

%formule PR per ricavare hr alle due diverse T (FORMA VETTORIALE)
%NB SCRIVO SOLO I DATI RELATIVI AL COMPOSTO A, L'UNICO PER CUI DEVO FARE IL
%BILANCIO DI ENERGIA PERCHE' L'UNICO INTERESSATO AL COMPRESSORE:
s=[0.37464 1.54226 -0.26992];
S=s(1)+s(2)*om(1)+s(3)*(om(1))^2; %scalare
Tr=T/Tc(1); %vettore delle Tc di A alle due diverse T (in e out)
Pr=P./Pc(1);
k=(1+S*(1-sqrt(Tr))).^2; %vettore
a=(0.45724*k*(R*Tc(1))^2)/Pc(1); %vettore
b=(0.07780*R*Tc(1))/Pc(1); %scalare
A=(a.*P)./(R*T).^2; %vettore
B=(b*P)./(R*T); %vettore
alfa=-1+B;
beta=A-(2*B)-3*(B).^2;
gamma=-(A.*B)+(B).^2+(B).^3; %tutti vettori, ciascuno con due componenti riferite alle due T in e out
coeff1=[1 alfa(1) beta(1) gamma(1)]; %NB L'ALGORITMO CHE RISOLVE LE CUBICHE VUOLE UN VETTORE RIGA NON UNA MATRICE, QUINDI RISOLVO SEPARATAMENTE
z1=roots(coeff1);
zv1=max(z1); 
coeff2=[1 alfa(2) beta(2) gamma(2)];
z2=roots(coeff2);
zv2=max(z2);
zv=[zv1 zv2];%vettore con i 2 zvapore alle due T
E=S*sqrt(Tr./k); %vettore
hrRT=zv-1-(A./(2*sqrt(2)*B)).*(1+E).*log((zv+B*(1+sqrt(2)))./(zv+B*(1-sqrt(2))));
hr=hrRT.*(R*T);

%definisco funz Cp e calcolo integrale
syms T
T1=350;
T2=400;
cpA=alfaCp(1)+betaCp(1)*T;
integrale=int(cpA,T2,T1);

%bilancio di energia:
W=F1*(hr(2)-hr(1)-integrale);
W=vpa(W);

%% FLASH CON REAZIONE
%dati:
F2=50; %mol/s
F=F1+F2; %mol/s
nu=[-2 -1 1];
T2=400; %K
P2=12; %bar
z=[0.67 0.33 0]; %frazioni molari corrente in entrata
n0=z*F; %mol/s (portate molari in ingresso)
G0frif=[1.223 0.3211 -1.372]*1e3; %J/mol
H0frif=[1.398 0.8612 -2.132]*1e3; %J/mol

%Kirchhoff: voglio H0RdiT/RT^2 (integranda)-->calcolo i cp singoli:
syms T t %uso T come variabile del primo integrale e t del secondo
Trif=298;
T2=400; %K
CpdiT=alfaCp+betaCp*T; %vettore [CpA CpB CpC] funz di T
DeltaCpdiT=sum(nu.*CpdiT); %funzione di T, scalare
H0Rrif=sum(H0frif.*nu);%prodotto scalare somma componente per componente
H0Rdit=H0Rrif+int(DeltaCpdiT,T,Trif,t); %funzione scalare di t, da integrare in Van'tHoff

%Van't Hoff:
G0Rrif=sum(G0frif.*nu); 
G0RrifRT=G0Rrif/(R*Trif);
integranda=H0Rdit/(R*t^2);
I_sim=int(integranda,t,Trif,T2);
I=eval(I_sim);%ti valuta la funzione in quel valore
G0RRT=G0RrifRT-I;
 
options=optimset('tolfun',1e-12,'maxiter',10000,'maxfunevals',10000);
[l,fval,exitflag]=fsolve(@eqchimico,25,options);

y



