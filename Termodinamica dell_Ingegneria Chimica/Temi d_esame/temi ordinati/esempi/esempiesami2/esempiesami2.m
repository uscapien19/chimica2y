close all
clear all
clc
global n0a n0b nua nub Keq x y
%%dati:
T=110+273; %K
p=5; %bar
% s=[A B C P S1 S2]
nua=[-2 1 1 0 0 0];
nub=[0 -1 -1 1 0 0];
n0a=[92.59 0 0 0 250 0];
n0b=[0 104.17 0 0 0 98.68];
Keq=[2.62 2.96];

%per risolvere il sistema imparo a memoria questa stringa:
options=optimset('display','iter','tolfun',1e-10,'maxiter',3e+3,'maxfunevals',10000);
[d,fval,exitflag]=fsolve(@esesami2_sistema,[46 50 50 50],options); %cos� risolve tenendo conto delle opzioni che ho definito prima
disp(['il grado di avanzamento 1 �',num2str(d(1))]);%2str1 riporta il risultato numerico che ottiene facnedo girare il sistema poi ci aggiunge l'etichetta
disp(['il grado di avanzamento 2 �',num2str(d(2))]);
disp(['il grado di avanzamento B �',num2str(d(2))]);
disp(['il grado di avanzamento C �',num2str(d(3))]);

x
y