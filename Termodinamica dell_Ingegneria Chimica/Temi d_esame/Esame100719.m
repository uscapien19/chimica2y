clc; clear all; close all;

%eseguito con delta=6

global F1 F2 k1 k2 tau R Tre Pre F3 z
Tre=623.15; %K
Pre=1e5; %Pa
tau=60; %s
k1=0.5; %s^-1
k2=0.01+6/100;
F1=1/3600*1e3; %mol/s
F2=1/3600*1e3;
R=8.314; %J/mol/K

%CSTR
sol0=[0.3 0.3 0.4 0.5];
options=optimset('MaxIter',30000,'MaxFunEvals',30000,'TolX',1e-8,'TolFun',1e-8,'Display','off');
sol=fsolve(@CSTR,sol0,options);
F3=sol(4) %mol/s
z=sol(1:3)

%TD
sol1=[0.25 0.25 320 0.5 0.5 0.1];
solT=fsolve(@TD,sol1,options);
La=solT(1) %mol/s
Lb=solT(2) %mol/s
Tf=solT(3) %K
xa=[solT(4) solT(5) 0]
xb=[0 solT(6) 0.9]

% Facoltativa
cl=[90 130 376]; %J/mol/K
dhev=[20 22 62]*1e3; %J/mol
Q=F3*(sum(z.*(cl*(Tf-Tre)-dhev)))

function f=CSTR(a)
global F1 F2 k1 k2 tau R Tre Pre
z=a(1:3); F3=a(4);
V=tau*F3*R*Tre/Pre;
C=z.*(Pre/(R*Tre));

f(1)=F1-F3*z(1)-k1*V*C(1);
f(2)=F2-F3*z(2)+(k2*C(3)-k1*C(1))*V;
f(3)=-F3*z(3)+V*(2*k1*C(1)-k2*C(3));
f(4)=sum(z)-1;
end

function f=TD(a)
global F3 z
La=a(1); Lb=a(2); Tf=a(3); xa=[a(4) a(5) 0]; xb=[0 a(6) 0.9];
beta=(900+10*6)/Tf;
gammaa=exp(beta*(1-xa(2))^2);
gammab=exp(beta*(1-xb(2))^2);

f(1)=F3-La-Lb; %BM tot
f(2)=xb(2)*gammab-xa(2)*gammaa; %VLE B
f(3)=sum(xa)-1;
f(4:6)=F3*z-La*xa-Lb*xb;
end




