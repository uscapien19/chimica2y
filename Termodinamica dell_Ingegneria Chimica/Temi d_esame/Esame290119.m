clc
close all
clear all

global nF Pf Tf Tr Pr zi dhf cp nui1 nui2 dgf P0i R
nF=1e3; %mol/s
Pf=2.66e5; %Pa
Tf=450; %K
Tr=298.15; %K
Pr=1e5; %Pa
zi=[0.3 0.15 0.5 0.05]; % O M P B
C1=[9.486 9.724 9.426 9.569];
C2=[-1765 -2001 -1669 -1573];
C3=[-24.78 13.02 -36.75 -14.71];
dhf=[19.08 16.32 18.03 82.88]*1e3; %j/mol
cp=[180 178 182 130]; %j/molK
nui1=[-1 0 1 0];
nui2=[0 -1 1 0];
dgf=[122 118.8 121.4 129.6]*1e3; %j/molK
P0i=10.^(C1+C2./(Tf+C3));
R=8.314;

x0=[280 100 0.3 0.3 0.3 0.1 0.3 0.3 0.3 0.1 500 500];
options=optimset('MaxFunEval',20000,'TolX',1e-8,'TolFun',1e-8,'MaxIter',20000);
sol=fsolve(@sistema,x0,options)

function y=sistema(x)
global nF Pf Tf Tr Pr zi dhf cp nui1 nui2 dgf P0i R
lam1=x(1);
lam2=x(2);
xi=[x(3) x(4) x(5) x(6)];
yi=[x(7) x(8) x(9) x(10)];
nL=x(11); nV=x(12);

y(1)=nF-nL-nV+sum(nui1*lam1)+sum(nui2*lam2);
y(2:5)=nF*zi-nV*yi-nL*xi+nui1*lam1+nui2*lam2;
y(6:9)=Pf*yi-P0i.*xi;
y(10)=sum(xi-yi);
kTr1=exp(-(sum(nui1.*dgf)/(R*Tr)));
kTr2=exp(-(sum(nui2.*dgf)/(R*Tr)));
y(11)=kTr1*exp(sum(nui1.*dhf/R.*(1/Tr-1/Tf)))-prod((Pf*yi/Pr).^nui1);
y(12)=kTr2*exp(sum(nui2.*dhf/R.*(1/Tr-1/Tf)))-prod((Pf*yi/Pr).^nui2);
end
