%% MEGA ODE PER GAIA
clear all
close all
clc
global C0 k1 k2
%% DATI
C0=[800/35 0 0];
k1=8e4/60*exp(-8000*4.186/(8.314*(100+273.15)));
k2=1e5/60*exp(-10000*4.186/(8.314*(100+273.15)));
tau=fsolve('megaODE_f',50);
[t,C]=ode23('megaODE_f2',[0 tau],C0);
Cf=C(length(t),:)
