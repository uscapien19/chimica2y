function yp=Batch_es1(t,y)
global nu k
A=y(1);
B=y(2);
C=y(3);
R=[k(1)*A;
k(2)*B];
r=nu*R;
yp=r;